package com.sparkTutorial.sparkSql.join

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{SparkSession, functions}

object MakerSpaces {

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)
    val session = SparkSession.builder().appName("MAkerSpaces").master("local[*]").getOrCreate()

    val maker = session.read.option("header", "true").csv("in/makerspaces-data.csv")
    val postCode = session.read.option("header", "true").csv("in/postcode.csv")
        .withColumn("PostCode", functions.concat_ws("",functions.col("PostCode"), functions.lit("")))

    System.out.println("Registros file makerspace")
    maker.select("Name", "Postcode").show(10,false)

    System.out.println("Registros file Postcode")
    postCode.show(10,false)

    val joined = maker.join(postCode, maker.col("Postcode").startsWith(postCode.col("Postcode")), "left_outer")
        joined.show(10,false)

    joined.groupBy("Region").count().show(100,false)

    postCode.createTempView("TmpPostCode")
    maker.createTempView("TmpMaker")

    val joined2 = session.sql("SELECT A.name,B.Postcode FROM TmpMaker A LEFT JOIN TmpPostCode B ON (A.PostCode = B.PostCode )")
    joined2.show(10,false)

  }
}
