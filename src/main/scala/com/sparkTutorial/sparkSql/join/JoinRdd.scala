package com.sparkTutorial.sparkSql.join

import org.apache.spark.{SparkConf, SparkContext}

object JoinRdd {

    def main(args: Array[String]) {

        val conf = new SparkConf().setAppName("JoinRDD").setMaster("local[1]")
        val sc = new SparkContext(conf)

        val nameAges = sc.parallelize(List(("Jose",31),("Ana",32)))
        val nameAddresses = sc.parallelize(List(("Manuel","CDMX"),("Ana","Qro")))

        val join = nameAges.join(nameAddresses)
        join.saveAsTextFile("out/age_join.txt")

        val leftJoin = nameAges.leftOuterJoin(nameAddresses)
        leftJoin.saveAsTextFile("out/left_join.txt")

        val rightJoin = nameAges.rightOuterJoin(nameAddresses)
        rightJoin.saveAsTextFile("out/right_join.txt")

        val fullJoin = nameAges.fullOuterJoin(nameAddresses)
        fullJoin.saveAsTextFile("out/full_join.txt")

    }
}
