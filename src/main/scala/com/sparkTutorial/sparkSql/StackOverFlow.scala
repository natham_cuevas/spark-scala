package com.sparkTutorial.sparkSql

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object StackOverFlow {

  val AGE_MIDPOINT = "age_midpoint"
  val SALARY_MIDPOINT = "salary_midpoint"
  val SALARY_MIDPOINT_BUCKET = "salary_midpoint_bucket"

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)
    val session = SparkSession.builder().appName("stackOverFlow").master("local[1]").getOrCreate()

    val dfReader = session.read
    val responses = dfReader
      .option("header","true")
      .option("inferSchema", value = true)
      .csv("in/stack-overflow-responses.csv")

    System.out.println(" Schema out ")
    responses.printSchema()

    val selectedColumns = responses.select("country","occupation", AGE_MIDPOINT, SALARY_MIDPOINT)
    selectedColumns.show(10,false)

    selectedColumns.filter(selectedColumns.col("country").=== ("Afghanistan")).show(false)

    val groupedDf = selectedColumns.groupBy("occupation")
    groupedDf.count().show(false)

    selectedColumns.filter((selectedColumns.col(AGE_MIDPOINT) < 20)).show(false)
    selectedColumns.orderBy(selectedColumns.col(SALARY_MIDPOINT).desc).show(10,false)

    System.out.print(" Group by country and aggregate by average salary")
    val dfGroupByCountry = selectedColumns.groupBy("country")
    dfGroupByCountry.avg(SALARY_MIDPOINT).show(20,false)

    val responseSalaryBucket = responses.withColumn(SALARY_MIDPOINT_BUCKET,
    responses.col(SALARY_MIDPOINT).divide(20000).cast("integer").multiply(20000))

    responseSalaryBucket.select(SALARY_MIDPOINT,SALARY_MIDPOINT_BUCKET).show(20,false)

  }
}