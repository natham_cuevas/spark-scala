package com.sparkTutorial.sparkSql

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._

object SqlExample {

   case class Person(name: String, age: Long)
   def main(args: Array[String]): Unit = {

      Logger.getLogger("org").setLevel(Level.ERROR)
      val spark = SparkSession.builder().appName("Sql Example").master("local[2]").getOrCreate()

      runRddExample(spark)
      runDataframeExample(spark)
      runDatasetExample(spark)

     spark.stop()
   }

   private def runRddExample(spark: SparkSession): Unit = {

      import spark.implicits._
      val peopleRdd = spark.sparkContext.textFile("in/people.txt")
      val schemaRdd = "name age"
      val fields = schemaRdd.split(" ")
        .map(fieldName => StructField(fieldName, StringType, nullable = true))
      val schema = StructType(fields)

      val rowRdd = peopleRdd
        .map(_.split(","))
        .map(attributes => Row(attributes(0), attributes(1).trim))

      val peopleDf = spark.createDataFrame(rowRdd, schema)
          peopleDf.createOrReplaceTempView("example1")

      val result = spark.sql("SELECT name FROM example1")
       result.map(attributes => "Name: " + attributes(0)).show(false)
   }

   private def runDataframeExample(spark: SparkSession): Unit = {

      val df = spark.read.json("in/people.json")
          df.show(false)

      import spark.implicits._
      df.printSchema()

      df.select("name").show(false)
      df.select($"age", $"age" + 1).show(false)
      df.filter($"age" >= 21 ).show(false)
      df.groupBy("age").count().show(false)

      df.createOrReplaceTempView("example2")
      val slqDf = spark.sql("SELECT * FROM example2")
          slqDf.show(false)
   }

   private def runDatasetExample(spark: SparkSession): Unit = {

      import spark.implicits._
      val caseClassDs = Seq(Person("Natham", 30)).toDS()
      caseClassDs.show(false)

      val exampleDs = Seq(1,2,3).toDS()
      exampleDs.map(_ + 2).collect()
      exampleDs.show(false)

      val pathDs = "in/people.json"
      val peopleDs = spark.read.json(pathDs).as[Person]
      peopleDs.show(false)

      peopleDs.createOrReplaceTempView("example3")
      spark.sql("SELECT age FROM example3").show(false)
   }
}
