package com.sparkTutorial.sparkSql


object HousePriceProblem {

        /* Crea un programa Spark para leer los datos del archivo in/RealEstate.csv,
        agrupar por ubicación, agregar el precio promedio por metro cuadrado y ordenar por precio promedio por metro cuadrado.

        Generar una nueva columna ("columna") calculada del precio promedio por metro cuadrado.

        El conjunto de datos contiene los siguientes campos:
        1. MLS: Número de servicio de listado múltiple para la casa (identificación única).
        2. Ubicación: ciudad/pueblo donde está ubicada la casa.
        3. Precio: el precio de cotización más reciente de la casa (en dólares).
        4. Dormitorios: número de dormitorios.
        5. Baños: número de baños.
        6. Tamaño: tamaño de la casa en metros cuadrados.
        7. Precio/metro cuadrado: precio de la casa por metro cuadrado.
        8. Estado: tipo de venta.

        Cada campo está separado por comas.
        */
}
