package com.sparkTutorial.rdd.persist

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.storage.StorageLevel

object PersistExample {

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val conf  = new SparkConf().setAppName("reduce").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val inputInt = List (1, 2, 3, 4, 5)
    val intRdd = sc.parallelize(inputInt)

    intRdd.persist(StorageLevel.MEMORY_ONLY)

    val productRdd = intRdd.reduce((x, y) => x * y)
    println("Product is: " + productRdd)
    val countRdd = intRdd.count()
    println("Count is:" + countRdd)

  }
}
