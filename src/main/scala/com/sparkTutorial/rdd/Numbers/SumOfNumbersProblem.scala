package com.sparkTutorial.rdd.Numbers

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}

object SumOfNumbersProblem {

  def main(args: Array[String]) {

       /* Crearemos un programa Spark para leer los primeros 100 números primos del archivo in/nums.txt, he imprimiremos la suma de esos números en la consola.

       Cada fila del archivo de entrada contiene 10 números primos separados por espacios.
       */

       Logger.getLogger("org").setLevel(Level.OFF)

    val conf  = new SparkConf().setAppName("primeNumbers").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val lines = sc.textFile("in/nums.txt")
    val numbers = lines.flatMap(line => line.split("\\s+"))
    val validNumbers = numbers.filter(number => !number.isEmpty)
    val numbersInt = validNumbers.map(number => number.toInt)

    println("Sum is : " + numbersInt.reduce((x, y) => x + y))


  }
}
