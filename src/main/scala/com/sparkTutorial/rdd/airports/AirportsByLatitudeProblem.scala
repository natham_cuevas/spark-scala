package com.sparkTutorial.rdd.airports

import com.sparkTutorial.commons.Utils
import org.apache.spark.{SparkConf, SparkContext}

object AirportsByLatitudeProblem {

  def main(args: Array[String]) {

    /* Crear un programa Spark para leer los datos del aeropuerto desde in/airports.txt, encuentre todos los aeropuertos cuya latitud sea mayor a 40.
       Luego, envíe el nombre del aeropuerto y la latitud del aeropuerto a out/airports_by_latitude.txt.
       
       ID, nombre , ciudad principal , país donde se encuentra el aeropuerto, código ,Código , Latitud, Longitud, Altitud, Zona horaria
     */

     val conf = new SparkConf().setAppName("airports").setMaster("local[2]")
     val sc = new SparkContext(conf)

     val airports = sc.textFile("in/airports.txt")
     val airportsUSA = airports.filter(line => line.split(Utils.COMMA_DELIMITER)(6).toFloat >= 40)

     val airportsName = airportsUSA.map(line =>{
     val splits = line.split(Utils.COMMA_DELIMITER)
         splits(1) + "," + splits(6) })

    airportsName.saveAsTextFile("out/airports_latitud.txt")
  }
}
