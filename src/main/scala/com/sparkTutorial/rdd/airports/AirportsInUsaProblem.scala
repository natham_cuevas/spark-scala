package com.sparkTutorial.rdd.airports

import com.sparkTutorial.commons.Utils
import org.apache.spark.{SparkConf, SparkContext}

object AirportsInUsaProblem {
  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("airports").setMaster("local[2]")
    val sc = new SparkContext(conf)

    val airports = sc.textFile("in/airports.txt")
    val airportsInUSA  = airports.filter(line => line.split(Utils.COMMA_DELIMITER)(3) == "\"United States\"")

    val nameAirportAndCity = airportsInUSA.map(line => {
      val splits = line.split(Utils.COMMA_DELIMITER)
      splits(1) + ", " + splits(2)
    })
    nameAirportAndCity.saveAsTextFile("out/airports_usa.txt")
  }
}
