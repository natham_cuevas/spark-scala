# Curso Spark - Scala


## Indra


###### ¿Qué se aprenderá en el curso?
*•	Sobre la arquitectura de Apache Spark.  
•	La capacidad de trabajar con la abstracción principal de Apache Spark, RDDs.  
•	Implementar soluciones con Apache Spark 2.0, utilizando transformaciones y acciones en RDD y Spark SQL.  
•	Creación de aplicaciones con Spark a un clúster Hadoop YARN.  
•	Analizar datos estructurados y semiestructurados (Datasets) y Dataframes, y entender el funcionamiento de Spark SQL.  
•	Técnicas para optimizar trabajos de Apache Spark mediante el particionado, almacenamiento en caché y la persistencia de RDDs.  
•	Sobre buenas prácticas de trabajo con Apache Spark.*