package com.sparkTutorial.rdd.WebLogs

import org.apache.spark.{SparkConf, SparkContext}

object UnionLogProblem {

  def main(args: Array[String]) {

       /* El archivo "in/nasa_19950701.csv" contiene 10000 líneas de registro para el 1 de julio.
       El archivo "in/nasa_19950801.csv" contiene 10000 líneas de registro para el 1 de agosto.
       vamos a generar un nuevo RDD que contenga las líneas de registro tanto del 1 de julio como del 1 de agosto.
       tomaremos una muestra del 10 % de las líneas de registro y las guárdaremos en el archivo "out/sample_logs.csv".

       Debemos tomar en cuenta que los archivos contienen líneas de encabezado (host logname tiempo método url respuesta bytes).

       Verificaremos que el encabezado se eliminen en el RDD resultante.
       */

       val conf  = new SparkConf().setAppName("unionLogs").setMaster("local[*]")
       val sc = new SparkContext(conf)

       val julyLogs = sc.textFile("in/nasa_19950701.csv")
       val augustLogs = sc.textFile("in/nasa_19950801.csv")

       val aggLogLine = julyLogs.union(augustLogs)
       val cleanHeader = aggLogLine.filter(line => isNotHeader(line))
       val sample = cleanHeader.sample(withReplacement = true,fraction = 0.1)

       sample.saveAsTextFile("out/sample_logs.csv")

  }
   def isNotHeader(line: String): Boolean = !(line.startsWith("host") && line.contains("bytes"))
}
