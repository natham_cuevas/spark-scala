package com.sparkTutorial.rdd.WebLogs

object SameHostsProblem {

  def main(args: Array[String]) {

       /* El archivo "in/nasa_19950701.csv" contiene 10000 líneas de registro para el 1 de julio.
       El archivo "in/nasa_19950801.csv" contiene 10000 líneas de registro para el 1 de agosto.
       Crear un programa Spark para generar un nuevo RDD que contenga los hosts a los que se accede en AMBOS días.
       Guarde el RDD resultante en el archivo "out/logs_same_hosts.csv".

       Salida de ejemplo:
       vagabundo.vf.mmc.com
       www-a1.proxy.aol.com
       
       Debemos tomar en cuenta que los archivos contienen líneas de encabezado (host logname tiempo método url respuesta bytes).
       
       Verificaremos que el encabezado se eliminen en el RDD resultante.
       */
      
    val conf = new SparkConf().setAppName("sameHosts").setMaster("local[1]")
    val sc = new SparkContext(conf)

    val julyLogs = sc.textFile("in/nasa_19950701.csv")
    val augustLogs = sc.textFile("in/nasa_19950801.csv")

    val julyHosts = julyLogs.map(line => line.split("\t")(0))
    val augustHosts = augustLogs.map(line => line.split("\t")(0))

    val intersection = julyHosts.intersection(augustHosts)

    val cleanedIntersection = intersection.filter(host => host != "host")
    cleanedIntersection.saveAsTextFile("out/logs_same_hosts.csv")

  }
}
